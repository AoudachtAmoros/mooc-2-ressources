
**Module** : Algorithmique et programmation

**Séquence** : Instructions de base

**publiques cibles** : Tronc commun

**Notions liées** :variables, Instructions, lecture, écriture, affectation.

**Compétences** :
 - Maîtrise la démarche algorithmique 
 - Maîtrise des principes de base de l'algorithmique 

**Objectifs Pédagogiques** : 
-  Décrire les composantes d'un algorithme. 
-  Appliquer l’algorithme dans la résolution d'un problème spécifique simple
-  Montrer une attitude positive vis-à-vis de l'utilisation de l’algorithme dans la résolution des problèmes de la vie. 
- Déclarer correctement une donnée selon son nom, son type, sa nature, sa valeur.
  
**Auteur** : AOUDACHT Abdelkarim

**Durée de l’activité** :une heure .

**Forme de participation** :groupe.

**Préalables requis**  
  - Information
  - Traitement
  - Système inform
  
**Fiche élève cours** :Disponible [ici](https://drive.google.com/file/d/1wotyPTHOOz6Vgky8wHA4FYMmsvlkIVnO/view?usp=sharing)

**Fiche élève activité** :Disponible [ici](https://drive.google.com/file/d/1qoDsMBJZMwYOtlBOUPHAebNDsBooemTv/view?usp=sharing)

