
**Niveau et Durée** : 2 séance.

**Dans les programmes du niveau visé** : Algorithmique et programmation
- Variables et instruction élémentaires: écrire une formule permettant un calcul le factoriel d'un nombre ;
- Programmer une instruction conditionnelle ;
- Notion de boucle : Programmer des programmes qui exploites les boucles.
- Notion de fonction : Programmer des fonctions simples qui recevoir des arguments.
**TP** :
Activite pour trier des variables numériques.
